# board_game_break_the_code_telnet

Small game from board game "Break the Code" playable with paper and Telnet !

## How to use

`main.py [nb_players] [port]`

Each player must connect to `telnet IP PORT`

Follow the (french) instructions on server console

- Default nb_players : **2**
- Default port : **2609**