import random
import socket
import sys
from functools import cmp_to_key


def compare(chaine1, chaine2):
    a = chaine1[0]
    b = chaine2[0]
    if a < b:
        return -1  # chaine1 first
    elif a > b:
        return 1  # chaine2 first
    elif a == b:
        if 'noir' in chaine1:
            return -1
        elif 'noir' in chaine2:
            return 1
        elif chaine1 == chaine1:
            return 0
        else:
            raise "Erreur de tri"


def tri_liste_tuiles(liste):
    liste.sort(key=cmp_to_key(compare))


def jeu(nombre_de_joueurs, port_serveur):
    print(f"Jeu pour {nombre_de_joueurs} joueurs sur le port {port_serveur}")

    # initialisation de la partie réseau
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(('', port_serveur))  # on va écouter sur ce port
    s.listen()  # on ouvre la connexion

    liste_chiffres_disponibles = []
    for i in range(0, 10):
        if i != 5:
            liste_chiffres_disponibles.append(f'{i} blanc')
            liste_chiffres_disponibles.append(f'{i} noir')

        else:
            liste_chiffres_disponibles.append("5 vert")
            liste_chiffres_disponibles.append("5 vert")

    random.shuffle(liste_chiffres_disponibles)

    liste_questions = [
        'Somme des chiffres ?',
        'Somme des 3 premiers chiffres ?',
        'Somme des 3 deniers chiffres ?',
        'Somme des 3 chiffres centraux ?',
        'Somme des chiffres blancs ?',
        'Somme des chiffres noirs ?',
        'Somme des chiffres pairs ?',
        'Somme des chiffres impairs ?',
        'Nombre de chiffres identiques ?',
        'Nombre de chiffres blancs ?',
        'Nombre de chiffres noirs ?',
        'Nombre chiffres pairs ?',
        'Nombre chiffres impairs ?',
        'Emplacement des 0 ?',
        'Emplacement des 5 ?',
        'Emplacement des 1 OU des 2 (a choisir) ?',
        'Emplacement des 6 OU des 7 (a choisir) ?',
        'Emplacement des 8 OU des 9 (a choisir) ?',
        'Emplacement des 3 OU des 4 (a choisir) ?',
        'Emplacement des chiffres qui se suivent ?',
        'Emplacement des chiffres de memes couleurs ?',
        'Emplacement des chiffres identiques ?',
        'Le troisieme chiffre est strictement superieur a 4 ?',
        'Difference entre le plus grand chiffre et le plus petit chiffre ?',
    ]

    random.shuffle(liste_questions)

    if nombre_de_joueurs == 2:
        nombre_tuiles_joueur = 5
        nombre_tuiles_table = 0
    elif nombre_de_joueurs == 3:
        nombre_tuiles_joueur = 5
        nombre_tuiles_table = 5
    elif nombre_de_joueurs == 4:
        nombre_tuiles_joueur = 4
        nombre_tuiles_table = 4
    else:
        raise "Nombre de joueurs incorrect"

    liste_tuiles_table = []
    for i in range(0, nombre_tuiles_table):
        liste_tuiles_table.append(liste_chiffres_disponibles.pop(0))
        tri_liste_tuiles(liste_tuiles_table)
    conn_liste = []
    for joueur in range(0, nombre_de_joueurs):
        liste_tuiles_joueur = []
        print(f'Attente du joueur {joueur + 1}')
        conn, addr = s.accept()  # on attent qu'un joueur se connecte
        conn_liste.append(conn)  # on enregistre l'object pour communiquer avec lui
        for i in range(0, nombre_tuiles_joueur):
            liste_tuiles_joueur.append(liste_chiffres_disponibles.pop(0))
        tri_liste_tuiles(liste_tuiles_joueur)
        conn.send(f"\r\nHello joueur {joueur + 1} !\r\nVoici vos chiffres : \r\n".encode())
        for chiffre in liste_tuiles_joueur:
            conn.send((chiffre + '\r\n').encode())

    joueur_en_cours = random.randint(0, nombre_de_joueurs - 1)

    while True:
        liste_questions_possibles = liste_questions[:6]

        print(f'Au tour du joueur {joueur_en_cours + 1}')
        print(f"Voici la liste des questions possibles {len(liste_questions_possibles)}/{len(liste_questions)} :")
        for (i, question) in enumerate(liste_questions_possibles):
            print(f'{i + 1} : {question}'.encode())

        for i_joueur, conn in enumerate(conn_liste):
            conn.send(f'\r\nAu tour du joueur {joueur_en_cours + 1}\r\n'.encode())
            if i_joueur == joueur_en_cours:
                conn.send("C'est ton tour !\r\n".encode())
            conn.send(
                f"Voici la liste des questions possibles {len(liste_questions_possibles)}/{len(liste_questions)} : \r\n".encode())
            for (i, question) in enumerate(liste_questions_possibles):
                conn.send(f'{i + 1} : {question}\r\n'.encode())

        key = input("Numéro de question à supprimer ou ENTREE si vous avez deviné le code : ")
        if '1' <= key <= '9':
            question_choisie = liste_questions.pop(int(key) - 1)
            print(f'Question choisie par joueur {joueur_en_cours + 1} : {question_choisie}')
            for conn in conn_liste:
                conn.send(f'\r\nQuestion choisie par joueur {joueur_en_cours + 1} : {question_choisie}\r\n'.encode())
            if liste_questions:  # s'il reste des cartes questions
                joueur_en_cours += 1
                joueur_en_cours %= nombre_de_joueurs
                continue  # on ne va pas plus loin et on reboucle

        input("Appuyer sur ENTREE si vous avez deviné le code : ")
        if nombre_tuiles_table != 0:
            print("Voici les chiffres qu'il fallait deviner :")
            for chiffre in liste_tuiles_table:
                print(chiffre)

            for conn in conn_liste:
                conn.send("\r\nVoici les chiffres qu'il fallait deviner : \r\n".encode())
                for chiffre in liste_tuiles_table:
                    conn.send((chiffre + '\r\n').encode())

        for conn in conn_liste:
            conn.send('\r\nBye bye !\r\n'.encode())

        input("Appuyer sur ENTREE pour quitter")
        break  # on sort de la boucle


if __name__ == '__main__':
    nombre_joueurs = 2
    port = 2609

    if len(sys.argv) >= 2 and sys.argv[1].isnumeric():  # on regarde si on a le nombre de joueurs en paramètre
        nombre_joueurs = int(sys.argv[1])
    if len(sys.argv) >= 3 and sys.argv[2].isnumeric():  # on regarde si on a le port en paramètre
        port = int(sys.argv[2])

    jeu(nombre_joueurs, port)
